import bookstore.dao.Author;
import bookstore.dao.Book;
import bookstore.ds.AuthorDs;
import bookstore.ds.BookDs;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
class BookStoreControllerTest {

    @Mock
    private Book book;
    @Mock
    private BookDs bookDs;
    @Mock
    private Author author;
    @Mock
    private AuthorDs authorDs;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addBook() {
        bookDs.setTitle("Test");
        when(bookDs.getTitle()).thenReturn("Test");
        assertEquals("Test", bookDs.getTitle());

        bookDs.setReleaseDate(new Date(2015-10-25));
        when(bookDs.getReleaseDate()).thenReturn(new Date(2015-10-25));
        assertEquals(new Date(2015-10-25), bookDs.getReleaseDate());

        bookDs.setLoan(false);
        when(bookDs.isLoan()).thenReturn(false);
        assertEquals(false, bookDs.isLoan());

        authorDs.setAuthor("Test Testowy");
        when(authorDs.getAuthor()).thenReturn("Test Testowy");
        assertEquals("Test Testowy", authorDs.getAuthor());
    }

    @Test
    void getAll() {
        book.findAll();
        verify(book).findAll();
    }

    @Test
    void getAllByTitle() {
        book.findByTitle(anyString());
        verify(book).findByTitle(anyString());
    }

    @Test
    void loanBook() {
        book.modifyLoan(anyString(), anyBoolean());
        verify(book).modifyLoan(anyString(), anyBoolean());
    }

    @Test
    void returnBook() {
        book.modifyLoan("Test", true);
        verify(book).modifyLoan("Test", true);
    }

    @Test
    void modifyBookTitle() {
        book.modifyTitle("Test", "Nowy Test");
        verify(book).modifyTitle("Test", "Nowy Test");
    }

    @Test
    void modifyReleaseDate() {
        book.modifyReleaseDate("Test", new Date(2020-01-01));
        verify(book).modifyReleaseDate("Test", new Date(2020-01-01));
    }

    @Test
    void modifyAuthor() {
        author.modifyName(1, "Test Testowy");
        verify(author).modifyName(1, "Test Testowy");
    }

    @Test
    void deleteBook() {
        book.deleteByTitle("Test");
        author.delete(bookDs.getAuthor());
        verify(book).deleteByTitle("Test");
        verify(author).delete(bookDs.getAuthor());
    }
}