package bookstore.service;

import bookstore.dao.User;
import bookstore.ds.UserDs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Serwis do obsługi użytkowników z użyciem DAO
 */

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private User userDao;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public org.springframework.security.core.userdetails.User loadUserByUsername(String username) throws UsernameNotFoundException {
        List<GrantedAuthority> authorities = new ArrayList<>();
        UserDs user = userDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Brak użytkownika o loginie " + username);
        }
        authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRole().toUpperCase()));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }
}