package bookstore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Klasa uruchomieniowa aplikacji
 */
@SpringBootApplication
public class Application {

	private static Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		logger.info(String.format("Uruchamiam aplikację. Klasa startowa %s", Application.class.getSimpleName()));
		SpringApplication.run(Application.class, args);
	}
}
