package bookstore.exceptions;

import bookstore.helper.HeaderHelper;
import org.hibernate.HibernateException;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;
import java.text.ParseException;

/**
 * klasa przechwytująca wyjątki aplikacyjne
 */
@RestControllerAdvice
public class ExceptionsHandler extends Exception {

    private Logger logger = LoggerFactory.getLogger(ExceptionsHandler.class);

    @Autowired
    private HeaderHelper headerHelper;

    public ExceptionsHandler() {
        logger.info(String.format("Startująca klasa %s", ExceptionsHandler.class.getSimpleName()));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity constraintError(Exception e) {
        logger.error("Błąd na poziomie integracji danych w bazie danych: " + e.getLocalizedMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .headers(headerHelper.headers())
                .body("Błąd na poziomie integracji danych w bazie danych");
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity npeError(Exception e) {
        logger.error("Wystąpił null pointer. Sprawdź przesyłane dane: " + e.getLocalizedMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .headers(headerHelper.headers())
                .body("Wystąpił null pointer. Sprawdź przesyłane dane");
    }

    @ExceptionHandler(ParseException.class)
    public ResponseEntity parseError(Exception e) {
        logger.error("Błąd przetwarzania danych: " + e.getLocalizedMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .headers(headerHelper.headers())
                .body("Błąd przetwarzania danych");
    }

    @ExceptionHandler(SQLException.class)
    public ResponseEntity sqlError(Exception e) {
        logger.error("Błąd zapytania do bazy danych: " + e.getLocalizedMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .headers(headerHelper.headers())
                .body("Błąd zapytania do bazy danych");
    }

    @ExceptionHandler(HibernateException.class)
    public ResponseEntity hibernateError(Exception e) {
        logger.error("Błąd hibernate: " + e.getLocalizedMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .headers(headerHelper.headers())
                .body("Błąd hibernate");
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity jsonError(Exception e) {
        logger.error("Błąd struktury komunikatu JSON: " + e.getLocalizedMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .headers(headerHelper.headers())
                .body("Błąd struktury komunikatu JSON");
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity argumentError(Exception e) {
        logger.error("Błąd argumentu metody. Wartość argumentu jest błędna: " + e.getLocalizedMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .headers(headerHelper.headers())
                .body("Błąd argumentu metody. Wartość argumentu jest błędna");
    }
}
