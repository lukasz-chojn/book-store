package bookstore.dto;

import bookstore.ds.AuthorDs;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * klasa walidująca dane przesłane od użytkownika dodającego książki
 */
public class BooksDto {

    private String title;
    private String author;
    private String releaseDate;
    private AuthorDs authorDs;

    @NotEmpty(message = "Tytuł książki jest wymagany")
    public String getTitle() {
        return title;
    }

    @NotEmpty(message = "Autor jest wymagany")
    @Pattern(regexp = "\\D+", message = "Dozwolone są jedynie znaki nie będące cyframi")
    public String getAuthor() {
        return author;
    }

    @NotEmpty(message = "Data jest wymagana")
    @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}", message = "Data musi być w formacie yyyy-mm-dd. Dozwolone są jedynie cyfry")
    public String getReleaseDate() {
        return releaseDate;
    }

    public AuthorDs getAuthorDs() {
        return authorDs;
    }

    public Date parseDate() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.parse(releaseDate);
    }
}
