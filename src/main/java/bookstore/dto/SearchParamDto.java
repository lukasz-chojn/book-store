package bookstore.dto;

public class SearchParamDto {

    private String title;
    private String author;
    private String newReleaseDate;
    private String newTitle;
    private String newAuthor;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getNewReleaseDate() {
        return newReleaseDate;
    }

    public void setNewReleaseDate(String newReleaseDate) {
        this.newReleaseDate = newReleaseDate;
    }

    public String getNewTitle() {
        return newTitle;
    }

    public void setNewTitle(String newTitle) {
        this.newTitle = newTitle;
    }

    public String getNewAuthor() {
        return newAuthor;
    }

    public void setNewAuthor(String newAuthor) {
        this.newAuthor = newAuthor;
    }
}
