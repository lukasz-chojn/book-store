package bookstore.constants;

/**
 * klasa zawierająca stałe zdefiniowane dla konfiguracji zabezpieczeń
 */
public class ConstantsSecurities {
    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;
}
