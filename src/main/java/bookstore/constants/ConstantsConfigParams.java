package bookstore.constants;

/**
 * klasa zawierająca stałe zdefiniowane dla klasy konfiguracyjnej aplikacji
 */
public class ConstantsConfigParams {
    public static final String CHARACTER_ENCODING = "UTF-8";
}
