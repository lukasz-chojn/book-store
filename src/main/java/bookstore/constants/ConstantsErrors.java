package bookstore.constants;

/**
 * klasa zawierająca stałe zdefiniowane dla sekcji błędów kontrolerze
 */
public class ConstantsErrors {

    public static final String EMPTY_OBJECT = "Przesłany obiekt jest pusty";
    public static final String TITLE_NOT_FOUND = "Tytuł nie może być pusty";
    public static final String BOOK_NOT_FOUND = "Nie znaleziono książki o podanym tytule i autorze";
    public static final String ERROR_SEARCHING_BOOK = "Wystąpił błąd przy wyszukiwaniu książek. Sprawdź czy wszystko jest w porządku z połączeniem lub danymi w bazie";
    public static final String ERROR_ADDING_BOOK = "Wystąpił błąd przy dodawaniu książki";
    public static final String BOOKS_DATABASE_EMPTY = "Baza książęk jest pusta";
    public static final String TITLE_AND_AUTHOR_REQUIRED = "Wymagane pola to: tytuł, oraz autor";
    public static final String BOOK_IS_LOAN_ALREADY = "Książka jest już wypożyczona";
    public static final String TITLE_NEW_TITLE_AND_AUTHOR_REQUIRED = "Wymagane pola to: tytuł, nowy tytuł oraz autor";
    public static final String DATE_REQUIRED = "Data jest wymagana";
    public static final String AUTHOR_REQUIRED = "Autor jest wymagany";
    public static final String ERROR_RETURN_BOOK = "Wystąpił problem ze zwrotem książki ";
}
