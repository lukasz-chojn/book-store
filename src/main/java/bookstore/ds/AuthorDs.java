package bookstore.ds;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Klasa konfiguracyjna tabeli z autorami
 */
@Entity
@Table(name = "author")
public class AuthorDs implements Serializable {

    private int id;
    private String author;
    private List<BookDs> books;

    @Column(name = "id", unique = true, nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "author", nullable = false)
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @OneToMany(mappedBy = "author")
    public List<BookDs> getBooks() {
        return books;
    }

    public void setBooks(List<BookDs> books) {
        this.books = books;
    }
}
