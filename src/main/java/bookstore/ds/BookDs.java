package bookstore.ds;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Konfiguracja tabeli z książkami
 */
@Entity
@Table(name = "books")
public class BookDs implements Serializable {

    private int id;
    private String title;
    private Date releaseDate;
    private boolean isLoan;
    private AuthorDs author;

    @Column(name = "id", unique = true, nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "title", nullable = false, unique = true, length = 150)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "release_date", nullable = false)
    @Temporal(TemporalType.DATE)
    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Column(name = "loan", nullable = false)
    public boolean isLoan() {
        return isLoan;
    }

    public void setLoan(boolean isLoan) {
        this.isLoan = isLoan;
    }

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author", referencedColumnName = "author", foreignKey = @ForeignKey(name = "author"), nullable = false)
    public AuthorDs getAuthor() {
        return author;
    }

    public void setAuthor(AuthorDs author) {
        this.author = author;
    }
}
