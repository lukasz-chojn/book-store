package bookstore.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

/**
 * klasa pomocnicza odpowiadająca za obsługę nagłówków http
 */
@Component
public class HeaderHelper {

    private Logger logger = LoggerFactory.getLogger(HeaderHelper.class);

    public HttpHeaders headers() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/html; charset=UTF-8");
        return headers;
    }
}
