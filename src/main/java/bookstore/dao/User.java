package bookstore.dao;

import bookstore.ds.UserDs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfejs do obsługi użytkowników
 */
@Repository
public interface User extends JpaRepository<UserDs, Integer> {

    UserDs findByUsername(String username);

}
