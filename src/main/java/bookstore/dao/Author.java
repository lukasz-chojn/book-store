package bookstore.dao;

import bookstore.ds.AuthorDs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Interfejs do obsługi tabeli z autorami
 */
@Repository
public interface Author extends JpaRepository<AuthorDs, Integer> {

    @Transactional
    @Modifying
    @Query(value = "UPDATE author SET author=?2 WHERE id=?1", nativeQuery = true)
    void modifyName(Integer id, String newName);

    @Query(value = "SELECT id FROM author WHERE author = ?1", nativeQuery = true)
    AuthorDs findAuthor(String author);
}
