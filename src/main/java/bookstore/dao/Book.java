package bookstore.dao;

import bookstore.ds.BookDs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Interfejs do obsługi klasy BookDs
 */
@Repository
public interface Book extends JpaRepository<BookDs, Integer> {

    List<BookDs> findByTitle(String title);

    @Query(value = "SELECT author FROM books WHERE title=?1", nativeQuery = true)
    String findAuthor(String title);

    @Query(value = "SELECT books.id, books.title, books.release_date, books.loan, books.author FROM books" +
            " JOIN author ON books.author=author.author", nativeQuery = true)
    List<Object[]> findAllBooksAndAuthors();

    @Query(value = "SELECT books.id, books.title, books.release_date, books.loan, books.author FROM books" +
            " JOIN author ON books.author=author.author WHERE title LIKE CONCAT('%',?1,'%')", nativeQuery = true)
    List<Object[]> findAllBooksAndAuthorsByTitle(String title);

    @Transactional
    void deleteByTitle(String title);

    @Transactional
    @Modifying
    @Query(value = "UPDATE books SET title=?2 WHERE title=?1", nativeQuery = true)
    void modifyTitle(String oldTitle, String newTitle);

    @Transactional
    @Modifying
    @Query(value = "UPDATE books SET loan=?2 WHERE title=?1", nativeQuery = true)
    void modifyLoan(String title, boolean isLoan);

    @Transactional
    @Modifying
    @Query(value = "UPDATE books SET release_date=?2 WHERE title=?1", nativeQuery = true)
    void modifyReleaseDate(String title, Date realeseDate);
}
