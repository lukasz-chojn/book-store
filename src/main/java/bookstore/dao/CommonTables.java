package bookstore.dao;

import bookstore.ds.BookDs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interfejs obsługujący obie tabele jednym zapytaniem
 */
@Repository
public interface CommonTables extends JpaRepository<BookDs, Integer> {

    @Query(value = "SELECT b.id, b.title, b.loan, b.release_date, a.author FROM books b JOIN author a ON b.author=a.author WHERE title = ?1 AND a.author = ?2", nativeQuery = true)
    List<BookDs> findBooksByTitleAndAuthor(String title, String author);
}
