package bookstore.configuration;

import bookstore.constants.ConstantsConfigParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Klasa konfiguracyjna aplikacji
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"bookstore"}, entityManagerFactoryRef = "entityManagerFactoryBean")
public class AppConfig implements WebMvcConfigurer {

    private Logger logger = LoggerFactory.getLogger(AppConfig.class);

    @Value("${driver}")
    private String databaseDriver;
    @Value("${url}")
    private String databaseUrl;
    @Value("${user}")
    private String databaseUsername;
    @Value("${pass}")
    private String databasePassword;
    @Value("${dialect}")
    private String databaseDialect;
    @Value("${hibernateDatabaseCreationMode}")
    private String hibernateDatabaseCreationMode;

    @Bean
    public DataSource dataSource() {
        logger.info(String.format("Inicjalizacja sterownika bazy danych: %s", DriverManagerDataSource.class.getSimpleName()));
        DriverManagerDataSource source = new DriverManagerDataSource();
        source.setDriverClassName(databaseDriver);
        source.setUrl(databaseUrl);
        source.setUsername(databaseUsername);
        source.setPassword(databasePassword);
        return source;
    }

    @Bean
    public DataSourceInitializer dataSourceInitializer() {
        logger.info(String.format("Podnoszę bean'a %s", DataSourceInitializer.class.getSimpleName()));

        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();

        logger.info(String.format("Dodaję dane startowe do aplikacji z użyciem %s", ResourceDatabasePopulator.class.getSimpleName()));

        resourceDatabasePopulator.addScript(new ClassPathResource("/db_data/start.sql"));
        resourceDatabasePopulator.addScript(new ClassPathResource("/db_data/user.sql"));

        dataSourceInitializer.setDataSource(dataSource());
        dataSourceInitializer.setDatabasePopulator(resourceDatabasePopulator);
        return dataSourceInitializer;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
        logger.info(String.format("Konfiguracja EntityManagera: %s", LocalContainerEntityManagerFactoryBean.class.getSimpleName()));
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        Properties properties = new Properties();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", hibernateDatabaseCreationMode);
        properties.put("hibernate.dialect", databaseDialect);
        properties.put("hibernate.connection.useUnicode", "true");
        properties.put("hibernate.connection.characterEncoding", ConstantsConfigParams.CHARACTER_ENCODING);
        properties.put("hibernate.connection.charSet", ConstantsConfigParams.CHARACTER_ENCODING);

        bean.setDataSource(dataSource());
        bean.setPackagesToScan("bookstore");
        bean.setPersistenceUnitName("bookstore");
        bean.setJpaVendorAdapter(jpaVendorAdapter());
        bean.setJpaProperties(properties);
        return bean;
    }

    @Bean
    public HibernateJpaVendorAdapter jpaVendorAdapter() {
        logger.info(String.format("Inicjalizacja i konfiguracja hibernate: %s", HibernateJpaVendorAdapter.class.getSimpleName()));
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setShowSql(true);
        adapter.setGenerateDdl(true);
        return adapter;
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        logger.info(String.format("Podnoszenie managera transakcji %s", JpaTransactionManager.class.getSimpleName()));
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setEntityManagerFactory(entityManagerFactoryBean().getObject());
        return manager;
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
        logger.info(String.format("Podnoszenie bean'a %s", ResourceBundleMessageSource.class.getSimpleName()));
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.addBasenames("messages/messages");
        source.setAlwaysUseMessageFormat(true);
        source.setDefaultEncoding(ConstantsConfigParams.CHARACTER_ENCODING);
        return source;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/webjars/**").addResourceLocations("/webjars/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }
}
