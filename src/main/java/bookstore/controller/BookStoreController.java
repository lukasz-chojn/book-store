package bookstore.controller;

import bookstore.constants.ConstantsErrors;
import bookstore.dao.Author;
import bookstore.dao.Book;
import bookstore.dao.CommonTables;
import bookstore.ds.AuthorDs;
import bookstore.ds.BookDs;
import bookstore.dto.BooksDto;
import bookstore.dto.SearchParamDto;
import bookstore.helper.HeaderHelper;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Kontroler do obsługi księgarni
 */
@RestController
@RequestMapping("/books")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BookStoreController {
    @Autowired
    private Author author;
    @Autowired
    private Book book;
    @Autowired
    private HeaderHelper helper;
    @Autowired
    private CommonTables commonTables;
    @Autowired
    private ResourceBundleMessageSource messageSource;

    private Logger logger = LoggerFactory.getLogger(BookStoreController.class);

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addBook(@RequestBody @Valid BooksDto booksDto, BindingResult bindingResult) throws ParseException {
        BookDs bookDs = new BookDs();
        AuthorDs authorDs = new AuthorDs();
        List<String> errorsProcessingData = new ArrayList<>();

        if (bindingResult.hasFieldErrors()) {
            for (FieldError error : bindingResult.getFieldErrors()) {
                errorsProcessingData.add(error.getDefaultMessage());
            }
            logger.error(String.format("%s%s%s", ConstantsErrors.ERROR_ADDING_BOOK, " ", errorsProcessingData.toString()));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(errorsProcessingData.toString());
        } else {

            logger.info(String.format("Zapisuję dane do tabel: %s%s%s", AuthorDs.class.getSimpleName(), " oraz ", BookDs.class.getSimpleName()));
            bookDs.setTitle(booksDto.getTitle());
            bookDs.setLoan(false);
            bookDs.setReleaseDate(booksDto.parseDate());
            bookDs.setAuthor(authorDs);
            authorDs.setAuthor(booksDto.getAuthor());
            authorDs.setBooks(Arrays.asList(bookDs));
            author.save(authorDs);
            book.save(bookDs);

            logger.info(messageSource.getMessage("BOOK_ADDED", new Object[]{booksDto.getTitle()}, Locale.getDefault()));
            return ResponseEntity.status(HttpStatus.OK).headers(helper.headers()).body(new Gson().toJson(messageSource.getMessage("BOOK_ADDED", new Object[]{booksDto.getTitle()}, Locale.getDefault())));
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Object[]> getAll() {

        if (book.findAllBooksAndAuthors().isEmpty()) {
            logger.error(ConstantsErrors.ERROR_SEARCHING_BOOK);
            ResponseEntity.status(HttpStatus.NOT_FOUND).headers(helper.headers()).body(ConstantsErrors.BOOKS_DATABASE_EMPTY);
        }

        return book.findAllBooksAndAuthors();
    }

    @GetMapping(value = "/book", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Object[]> getAllByTitle(@RequestParam(value = "title") String title) {

        if (title.equals("")) {
            logger.error(ConstantsErrors.EMPTY_OBJECT);
            ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.EMPTY_OBJECT);
        } else if (book.findAllBooksAndAuthorsByTitle(title).isEmpty()) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.TITLE_NOT_FOUND);
        } else {
            logger.info(String.format("Szukana książka: %s%s", title, " została znaleziona"));
            return book.findAllBooksAndAuthorsByTitle(title);
        }
        return Collections.emptyList();
    }

    @PutMapping(value = "/loanBook", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity loanBook(@RequestBody SearchParamDto searchParamDto) {

        boolean isLoan;

        if (searchParamDto == null
                || searchParamDto.getTitle().equals("")
                || searchParamDto.getAuthor().equals("")) {
            logger.error(ConstantsErrors.TITLE_AND_AUTHOR_REQUIRED);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.TITLE_AND_AUTHOR_REQUIRED);
        }

        List<BookDs> books = commonTables.findBooksByTitleAndAuthor(searchParamDto.getTitle(), searchParamDto.getAuthor());

        if (books.size() != 1) {
            logger.error(ConstantsErrors.BOOK_NOT_FOUND);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.BOOK_NOT_FOUND);
        } else {
            isLoan = books.get(0).isLoan();
        }

        if (isLoan) {
            return ResponseEntity.status(HttpStatus.CONFLICT).headers(helper.headers()).body(ConstantsErrors.BOOK_IS_LOAN_ALREADY);
        } else {
            book.modifyLoan(searchParamDto.getTitle(), true);
        }

        logger.info(messageSource.getMessage("BOOK_BORROWED", new Object[]{books.get(0).getTitle()}, Locale.getDefault()));
        return ResponseEntity.status(HttpStatus.OK).headers(helper.headers()).body(new Gson().toJson(messageSource.getMessage("BOOK_BORROWED", new Object[]{books.get(0).getTitle()}, Locale.getDefault())));
    }

    @PutMapping(value = "/returnBook", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity returnBook(@RequestBody SearchParamDto searchParamDto) {

        boolean canReturnBook;

        if (searchParamDto == null) {
            logger.error(ConstantsErrors.EMPTY_OBJECT);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.EMPTY_OBJECT);
        } else if (searchParamDto.getTitle().equals("") || searchParamDto.getAuthor().equals("")) {
            logger.error(ConstantsErrors.TITLE_AND_AUTHOR_REQUIRED);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.TITLE_AND_AUTHOR_REQUIRED);
        }

        List<BookDs> books = commonTables.findBooksByTitleAndAuthor(searchParamDto.getTitle(), searchParamDto.getAuthor());

        if (books.size() != 1) {
            logger.error(ConstantsErrors.BOOK_NOT_FOUND);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.BOOK_NOT_FOUND);
        } else {
            canReturnBook = books.get(0).isLoan();
        }

        if (canReturnBook) {
            book.modifyLoan(searchParamDto.getTitle(), false);
        } else {
            logger.error(String.format("%s%s", ConstantsErrors.ERROR_RETURN_BOOK, books.get(0).getTitle()));
            ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.ERROR_RETURN_BOOK + books.get(0).getTitle());
        }

        logger.info(messageSource.getMessage("BOOK_RETURNED", new Object[]{books.get(0).getTitle()}, Locale.getDefault()));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(new Gson().toJson(messageSource.getMessage("BOOK_RETURNED", new Object[]{books.get(0).getTitle()}, Locale.getDefault())));
    }

    @PutMapping(value = "/modifyTitle", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity modifyBookTitle(@RequestBody SearchParamDto searchParamDto) {

        if (searchParamDto == null) {
            logger.error(ConstantsErrors.EMPTY_OBJECT);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.EMPTY_OBJECT);
        } else if (searchParamDto.getTitle().equals("")
                || searchParamDto.getNewTitle().equals("")
                || searchParamDto.getAuthor().equals("")) {
            logger.error(ConstantsErrors.TITLE_NEW_TITLE_AND_AUTHOR_REQUIRED);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.TITLE_NEW_TITLE_AND_AUTHOR_REQUIRED);
        }

        List<BookDs> books = commonTables.findBooksByTitleAndAuthor(searchParamDto.getTitle(), searchParamDto.getAuthor());

        if (books.isEmpty()) {
            logger.error(ConstantsErrors.BOOK_NOT_FOUND);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.BOOK_NOT_FOUND);
        } else {
            book.modifyTitle(books.get(0).getTitle(), searchParamDto.getNewTitle());
        }

        logger.info(messageSource.getMessage("TITLE_HAS_BEEN_CHANGED", new Object[]{books.get(0).getTitle()}, Locale.getDefault()));
        return ResponseEntity.status(HttpStatus.OK).headers(helper.headers()).body(new Gson().toJson(messageSource.getMessage("TITLE_HAS_BEEN_CHANGED", new Object[]{books.get(0).getTitle()}, Locale.getDefault())));
    }

    @PutMapping(value = "/modifyDate", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity modifyReleaseDate(@RequestBody SearchParamDto searchParamDto) throws ParseException {

        if (searchParamDto == null) {
            logger.error(ConstantsErrors.EMPTY_OBJECT);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).headers(helper.headers()).body(ConstantsErrors.EMPTY_OBJECT);
        } else if (searchParamDto.getNewReleaseDate().equals("")) {
            logger.error(ConstantsErrors.DATE_REQUIRED);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.DATE_REQUIRED);
        }

        String dateFromJson = searchParamDto.getNewReleaseDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse(dateFromJson);

        List<BookDs> books = commonTables.findBooksByTitleAndAuthor(searchParamDto.getTitle(), searchParamDto.getAuthor());

        if (books.isEmpty()) {
            logger.error(ConstantsErrors.BOOK_NOT_FOUND);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.BOOK_NOT_FOUND);
        } else {
            book.modifyReleaseDate(books.get(0).getTitle(), date);
        }

        logger.info(messageSource.getMessage("DATE_HAS_BEEN_CHANGED", new Object[]{books.get(0).getTitle()}, Locale.getDefault()));
        return ResponseEntity.status(HttpStatus.OK).headers(helper.headers()).body(new Gson().toJson(messageSource.getMessage("DATE_HAS_BEEN_CHANGED", new Object[]{books.get(0).getTitle()}, Locale.getDefault())));
    }

    @PutMapping(value = "/modifyAuthor", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity modifyAuthor(@RequestBody SearchParamDto searchParamDto) {

        if (searchParamDto == null) {
            logger.error(ConstantsErrors.EMPTY_OBJECT);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.EMPTY_OBJECT);
        } else if (searchParamDto.getTitle().equals("")) {
            logger.error(ConstantsErrors.TITLE_NOT_FOUND);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).headers(helper.headers()).body(ConstantsErrors.TITLE_NOT_FOUND);
        } else if (searchParamDto.getAuthor().equals("")) {
            logger.error(ConstantsErrors.AUTHOR_REQUIRED);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.AUTHOR_REQUIRED);
        }

        List<BookDs> books = commonTables.findBooksByTitleAndAuthor(searchParamDto.getTitle(), searchParamDto.getAuthor());

        if (books.isEmpty()) {
            logger.error(ConstantsErrors.BOOK_NOT_FOUND);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.BOOK_NOT_FOUND);
        } else {
            author.modifyName(books.get(0).getId(), searchParamDto.getNewAuthor());
        }

        logger.info(messageSource.getMessage("AUTHOR_HAS_BEEN_CHANGED", new Object[]{books.get(0).getTitle()}, Locale.getDefault()));
        return ResponseEntity.status(HttpStatus.OK).headers(helper.headers()).body(new Gson().toJson(messageSource.getMessage("AUTHOR_HAS_BEEN_CHANGED", new Object[]{books.get(0).getTitle()}, Locale.getDefault())));
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteBook(@RequestBody SearchParamDto searchParamDto) {

        if (searchParamDto == null) {
            logger.error(ConstantsErrors.EMPTY_OBJECT);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(ConstantsErrors.EMPTY_OBJECT);
        } else if (book.findByTitle(searchParamDto.getTitle()).isEmpty()) {
            logger.error(ConstantsErrors.TITLE_NOT_FOUND);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).headers(helper.headers()).body(ConstantsErrors.TITLE_NOT_FOUND);
        }

        if (!book.findByTitle(searchParamDto.getTitle()).isEmpty()) {
            String bookAuthor = book.findAuthor(searchParamDto.getTitle());
            book.deleteByTitle(searchParamDto.getTitle());
            author.delete(author.findAuthor(bookAuthor));
        }

        logger.info(messageSource.getMessage("BOOK_DELETED", new Object[]{searchParamDto.getTitle()}, Locale.getDefault()));
        return ResponseEntity.status(HttpStatus.OK).headers(helper.headers()).body(new Gson().toJson(messageSource.getMessage("BOOK_DELETED", new Object[]{searchParamDto.getTitle()}, Locale.getDefault())));
    }
}
