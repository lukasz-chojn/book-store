TRUNCATE TABLE user;

INSERT INTO user (id, password, user_role, username) VALUES
(1, '$2a$10$Kj.clWM6a.J.HZ6MSsG8Z.nVfuNmmjPP.JOscS/8QyOD/CF83rXMG', 'admin', 'user');
