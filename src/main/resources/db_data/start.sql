TRUNCATE TABLE author;

insert into author (id, author) VALUES
(1, 'Łukasz C');


TRUNCATE TABLE books;

insert into books (id, loan, release_date, title, author) VALUES
(1, b'0', '2018-05-27', 'Nowa książka', 'Łukasz C');